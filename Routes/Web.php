<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

$config = [
    'middleware' => ['web'],
    'namespace'  => 'Themes\\Bootstrap\\Controllers'
];

Route::group($config, function (Router $router) {
    $router->get("/bs", "HomeController@index");
});
