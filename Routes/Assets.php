<?php

use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

$config = [
    'prefix' => 'assets/themes/bootstrap'
];

Route::group($config, function (Router $router) {
    $router->fallback(function (Filesystem $filesystem, $path) {

        // Get file path
        $filePath = base_path('themes/Bootstrap/Public/' . $path);
        $mimeType = $filesystem->mimeType($filePath);

        // Get file mimetype
        if (Str::endsWith($filePath, '.css')) {
            $mimeType = 'text/css';
        }

        // Get file mimetype
        if (Str::endsWith($filePath, '.js')) {
            $mimeType = 'text/javascript';
        }

        // Get file content
        $content = $filesystem->get($filePath);

        // Set mimetype and content of the response
        $response = response($content)->header('Content-Type', $mimeType);

        // If is production, add cache header
        if (config('app.env') === 'production') {
            $response = $response->header('Cache-Control', 'max-age=10000000');
        }

        // Send file
        return $response;
    });
});
