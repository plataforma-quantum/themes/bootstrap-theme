<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Bootstrap - Welcome to the theme</title>

    <link rel="stylesheet" href="{{url('/assets/themes/bootstrap/App.css')}}">

    <script src="{{url('/assets/themes/bootstrap/Turbolinks.js')}}"></script>
</head>

<body>
    @yield('content')

    <script src="{{url('/assets/themes/bootstrap/App.js')}}"></script>
    @if(config('app.env') == 'local')
    <script src="http://localhost:35729/livereload.js"></script>
    @endif
</body>

</html>
