<?php

namespace Themes\Bootstrap\Controllers;

use Illuminate\Routing\Controller;

class HomeController extends Controller
{

    /**
     * Render home page
     *
     */
    public function index()
    {
        return view('bs::Pages.Home');
    }
}
