<?php

namespace Themes\Bootstrap\Providers;

use Illuminate\Support\ServiceProvider;

class BootstrapThemeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/Assets.php');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/Web.php');
        $this->loadViewsFrom(__DIR__ . '/../Views', 'bs');
    }
}
